package com.elenappli.demo.controller;

import com.elenappli.demo.model.Contact;
import com.elenappli.demo.model.Member;
import com.elenappli.demo.service.MemberService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class MemberControllerTest {

    @Mock
    MemberService mockService;

    private MemberController controller;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        controller = new MemberController(mockService);
    }

    @Test
    public void Given_NoMembers_When_GetAllMembers_Then_ReturnEmptyList() {
        when(mockService.findAllMembers()).thenReturn(new ArrayList<>());
        ResponseEntity expectedResponse = new ResponseEntity<>(new ArrayList<>(), HttpStatus.OK);
        ResponseEntity actualResponse = controller.getAllMembers();
        assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void Given_Members_When_GetAllMembers_Then_ReturnListOfAllMembers() {
        List<Member> members = Arrays.asList(
                new Member("Ziggo", Collections.singleton(
                        new Contact("Bob", "bob@ziggo", "06 77711211"))),
                new Member("KPN", Stream.of(
                        new Contact("Joe", "joe@ziggo", "06 99911211"),
                        new Contact("Leo", "leo@ziggo", "06 88811211"))
                        .collect(Collectors.toSet())
                ));

        when(mockService.findAllMembers()).thenReturn(members);
        ResponseEntity expectedResponse = new ResponseEntity<>(members, HttpStatus.OK);
        ResponseEntity actualResponse = controller.getAllMembers();
        assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void Given_NewMember_When_PostMember_Then_ReturnNewCreatedMember() {
        Member m = new Member("KPN", Stream.of(
                new Contact("Joe", "joe@ziggo", "06 99911211"),
                new Contact("Leo", "leo@ziggo", "06 88811211"))
                .collect(Collectors.toSet()));
        when(mockService.createMember(m)).thenReturn(Optional.of(m));
        ResponseEntity expectedResponse = new ResponseEntity<>(m, HttpStatus.OK);
        ResponseEntity actualResponse = controller.postNewMember(m);
        assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void Given_MemberAlreadyExists_When_PostMember_Then_ReturnConflict() {
        Member m = new Member("KPN", Stream.of(
                new Contact("Joe", "joe@ziggo", "06 99911211"),
                new Contact("Leo", "leo@ziggo", "06 88811211"))
                .collect(Collectors.toSet()));
        when(mockService.createMember(m)).thenReturn(Optional.empty());
        ResponseEntity expectedResponse = new ResponseEntity<>(m, HttpStatus.CONFLICT);
        ResponseEntity actualResponse = controller.postNewMember(m);
        assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void Given_MemberDoesNotExists_When_GetMember_Then_ReturnNotFound() {
        Long id = 10L;
        when(mockService.findMemberById(id)).thenReturn(Optional.empty());
        ResponseEntity expectedResponse = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        ResponseEntity actualResponse = controller.getMemberById(id);
        assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void Given_MemberExists_When_GetMember_Then_ReturnMember() {
        Long id = 1L;
        Member m = new Member("Ziggo", Collections.singleton(
                new Contact("Bob", "bob@ziggo", "06 77711211")));
        when(mockService.findMemberById(id)).thenReturn(Optional.of(m));
        ResponseEntity expectedResponse = new ResponseEntity<>(m, HttpStatus.OK);
        ResponseEntity actualResponse = controller.getMemberById(id);
        assertEquals(expectedResponse, actualResponse);
    }
}