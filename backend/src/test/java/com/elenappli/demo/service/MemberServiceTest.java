package com.elenappli.demo.service;

import com.elenappli.demo.model.Contact;
import com.elenappli.demo.model.Member;
import com.elenappli.demo.repository.ContactRepository;
import com.elenappli.demo.repository.MemberRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class MemberServiceTest {

    @Mock
    ContactRepository mockContactRepo;

    @Mock
    MemberRepository mockMemberRepo;

    private MemberService service;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        service = new MemberService(mockContactRepo, mockMemberRepo);
    }

    @Test
    public void Given_NoMembers_When_FindAllMembers_Then_ReturnEmptyList() {
        when(mockMemberRepo.findAll()).thenReturn(new ArrayList<>());
        List<Member> actualMembers = service.findAllMembers();
        assertEquals(new ArrayList<>(), actualMembers);
    }

    @Test
    public void Given_Members_When_FindAllMembers_Then_ReturnListOfAllMembers() {
        List<Member> members = Arrays.asList(
                new Member("Ziggo", Collections.singleton(
                        new Contact("Bob", "bob@ziggo", "06 77711211"))),
                new Member("KPN", Stream.of(
                        new Contact("Joe", "joe@ziggo", "06 99911211"),
                        new Contact("Leo", "leo@ziggo", "06 88811211"))
                        .collect(Collectors.toSet())
                ));
        when(mockMemberRepo.findAll()).thenReturn(members);
        List<Member> actualMembers = service.findAllMembers();
        assertEquals(members, actualMembers);
    }

    @Test
    public void Given_MemberAlreadyExists_When_PostMember_Then_Empty() {
        Member m = new Member("Ziggo", Collections.singleton(
                new Contact("Bob", "bob@ziggo", "06 77711211")));
        when(mockMemberRepo.findByName(m.getName())).thenReturn(Optional.of(m));
        Optional<Member> actualMember = service.createMember(m);
        assertEquals(Optional.empty(), actualMember);
    }

    @Test
    public void Given_MemberDoesNotExists_When_GetMember_Then_ReturnNotFound() {
        Long id = 10L;
        when(mockMemberRepo.findById(id)).thenReturn(Optional.empty());
        Optional<Member> actualMember = service.findMemberById(id);
        assertEquals(Optional.empty(), actualMember);
    }

    @Test
    public void Given_MemberExists_When_GetMember_Then_ReturnMember() {
        Long id = 1L;
        Member m = new Member("Ziggo", Collections.singleton(
                new Contact("Bob", "bob@ziggo", "06 77711211")));
        when(mockMemberRepo.findById(id)).thenReturn(Optional.of(m));
        Optional<Member> actualMember = service.findMemberById(id);
        assertEquals(Optional.of(m), actualMember);
    }
}