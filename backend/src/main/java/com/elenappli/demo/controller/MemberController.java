package com.elenappli.demo.controller;

import com.elenappli.demo.model.Member;
import com.elenappli.demo.service.MemberService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@RestController
public class MemberController {

    @NotNull
    private MemberService memberService;

    public MemberController(@NotNull MemberService memberService) {
        this.memberService = memberService;
    }

    @GetMapping("/members")
    public ResponseEntity<List<Member>> getAllMembers() {
        return new ResponseEntity<>(memberService.findAllMembers(), HttpStatus.OK);
    }

    @PostMapping("/members")
    public ResponseEntity<Member> postNewMember(@Valid @RequestBody Member member) {
        Optional<Member> m = memberService.createMember(member);
        return m.map(value -> new ResponseEntity<>(value, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(member, HttpStatus.CONFLICT));
    }

    @GetMapping("/members/{memberId}")
    public ResponseEntity<Member> getMemberById(@PathVariable Long memberId) {
        Optional<Member> m = memberService.findMemberById(memberId);
        return m.map(value -> new ResponseEntity<>(value, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

}