package com.elenappli.demo.service;

import com.elenappli.demo.exception.ResourceNotFoundException;
import com.elenappli.demo.model.Contact;
import com.elenappli.demo.model.Member;
import com.elenappli.demo.repository.ContactRepository;
import com.elenappli.demo.repository.MemberRepository;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MemberService {
    @NotNull
    private ContactRepository contactRepository;

    @NotNull
    private MemberRepository memberRepository;

    public MemberService(ContactRepository contactRepository, MemberRepository memberRepository) {
        this.contactRepository = contactRepository;
        this.memberRepository = memberRepository;
    }

    public List<Member> findAllMembers() {
        List<Member> members = new ArrayList<>();
        memberRepository.findAll().forEach(members::add);
        return members;
    }

    public Optional<Member> findMemberById(Long id) {
        return memberRepository.findById(id);
    }

    public Optional<Member> createMember(Member member) {
        Optional<Member> checkMember = memberRepository.findByName(member.getName());
        if (checkMember.isPresent())
            return Optional.empty();

        memberRepository.save(member);
        for (Contact contact : member.getContacts()) {
            createContact(member.getId(), contact);
        }

        return memberRepository.findById(member.getId());
    }

    private void createContact(Long memberId, Contact contact) {
        memberRepository.findById(memberId).map(member -> {
            contact.setMember(member);
            return contactRepository.save(contact);
        }).orElseThrow(() -> new ResourceNotFoundException("MemberId " + memberId + " not found"));
    }
}
