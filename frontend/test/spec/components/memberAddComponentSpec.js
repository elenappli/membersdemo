'use strict';

describe('Component: MemberAdd', function () {

  var component, memberResource, $q, $compile, $scope;
  var mockMemberResponse = {
    name: "test-name",
    contact: {
      name: 'test-contact-name',
      email: 'test-contact-email',
      phone: 'test-contact-phone'
    }
  };

  beforeEach(function () {
    module('memberApp');
    inject(function (_$componentController_, _MemberResource_, _$q_, _$compile_, _$rootScope_) {
      $q = _$q_;
      $scope = _$rootScope_.$new();
      $compile = _$compile_;
      memberResource = _MemberResource_;
      component = _$componentController_('memberAdd', {
        MemberResource: memberResource
      });
    });
  });

  it('should successfully make request to add member', function () {
    spyOn(memberResource, "save").and.callFake(function(params, successCallBack) {
      successCallBack({
        name: 'test-name',
        id: 'test-id'
      });
    });

    component.memberName = mockMemberResponse.name;
    component.contactName = mockMemberResponse.contact.name;
    component.contactEmail = mockMemberResponse.contact.email;
    component.contactPhone = mockMemberResponse.contact.phone;

    expect(component.successMsg).toBeUndefined();
    expect(component.successId).toBeUndefined();

    component.addMember(); // method under test

    expect(memberResource.save).toHaveBeenCalledWith({
      name:component.memberName,
      contact: {
        name:component.contactName,
        email:component.contactEmail,
        phone:component.contactPhone
      }
    }, jasmine.any(Function), jasmine.any(Function));

    expect(component.successMsg.indexOf('test-name')).not.toBe(-1);
    expect(component.successId).toBe("test-id");

  });

  it('should handle failed request to add member', function () {
    spyOn(memberResource, "save").and.callFake(function(params, successCallBack, failureCallBack) {
      failureCallBack({
        statusText: '404',
        data: 'not found'
      });
    });

    component.name = mockMemberResponse.name;
    component.contactName = mockMemberResponse.contact.name;
    component.contactEmail = mockMemberResponse.contact.email;
    component.contactPhone = mockMemberResponse.contact.phone;

    expect(component.errorMsg).toBeUndefined();

    component.addMember(); // method under test

    expect(memberResource.save).toHaveBeenCalledWith({
      name:component.memberName,
      contact: {
        name:component.contactName,
        email:component.contactEmail,
        phone:component.contactPhone
      }
    }, jasmine.any(Function), jasmine.any(Function));

    expect(component.errorMsg.indexOf('404')).not.toBe(-1);
    expect(component.errorMsg.indexOf('not found')).not.toBe(-1);

  });

  // TODO implement the test that checks that the <member-add></member-add> component when used renders the add member
  // form
  xit('should render component', function() {

  });
});