
angular.module('memberApp').component('memberAdd', {
  templateUrl: 'component/member-add/member-add-view.html',
  controller: memberAddController
});

memberAddController.$inject = ['MemberResource'];
function memberAddController(MemberResource) {
  var vm = this;

  vm.addMember = function() {
    var onSuccess = function(successResponse) {
      vm.successMsg = successResponse.name + ' successfully added.';
      vm.successId = successResponse.id;
    };

    var onFailure = function (errorResponse) {
      vm.errorMsg = errorResponse.statusText + '(' + errorResponse.data + ')';
    };

    MemberResource.save({
      name: vm.memberName,
      contact: {
        name: vm.contactName,
        email: vm.contactEmail,
        phone: vm.contactPhone
      }
    }, onSuccess, onFailure);
  }
}