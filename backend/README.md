### How to Compile
./mvnw clean compile
### How to run tests
./mvnw test
### How to run application 
./mvnw spring-boot:run

## Notes
If the database schema ever changes then I would consider
creating a matching MemberDto and ContactDto from the Member
Entity and Contact Entity, but since they are the same currently
 I only created one representation of a Member and Contact
